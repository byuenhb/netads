#!/bin/bash

set -e

cat /root/hosts >> /etc/hosts
zypper refs && zypper ref
zypper --non-interactive in sssd ssd-krb5 python3-sssd-config sssd-ad sssd-tools krb5-client